# Café Forge

La devise : "Parlons Dev, parlons Ops, parlons Forge"

## Les épisodes

**Le prochain épisode aurra lieu mardi 5 juillet de 13h30 à 14h30**


* épisode 1 : Mardi 5 avril 2022
* épisode 2 : jeudi 21 avril 2022
* épisode 3 : mardi 3 mai 2022
* épisode 4 : jeudi 19 mai 2022
* épisode 5 : mardi 7 juin 2022
* époside 6 : jeudi 16 juin 2022

## Pour discuter entre 2 cafés

https://team.forgemia.inra.fr/miat-saab/channels/town-square

## Pour prendre des notes

https://hackmd.io/HLUu225WTo6vT9-wXETQpg

## L'esprit du Café Forge

Une petite animation à l'échelle de l'unité, pour commencer.

**Le premier mardi et le troisième jeudi de chaque mois de 13h30 à 14h30**,
la salle de réunion MIAT devient le Café Forge.

Qu'est ce que c'est?
-  un lieu de discussion très libre autour des utilisations des forges logicielles.


- Si vous voulez savoir à quoi ça sert,
- si vous voulez en savoir plus,
- si vous êtes intéressé par la gestion de codes,
- si vous êtes intéressé par la gestion de projet,
- si vous êtes intéressé par l'automatisation de vos processus,
- & aussi si vous avez un peu d'expérience à partager,


n'hésitez pas à venir au Café Forge pour discuter.

Ce serra aussi l'occasion d'aller visiter toutes les merveilleuses 
ressources disponibles sur internet,
ou de tenter des choses en direct.

- Venez avec ou sans votre PC,
- venez à n'importe quel moment entre 13h30 et 14h30,
- venez 5 minutes, venez 1 heure,
- venez avec vos idées,
- venez avec vos questions,
- quelque-chose que vous ne savez pas faire,
- quelque-chose que vous savez faire,
- c'est "Open Bar".  :)

## en marge du Café Forge

le projet de site web statique des notices du C8 : https://miat-com.pages.mia.inra.fr/c8-notices/

